import { summon } from "zalgo-js";
import audioURL from "./assets/audio.ogg";
import impulseURL from "./assets/impulse.ogg";

async function parse(ctx, url) {
  const r = await fetch(url);
  const data = await r.arrayBuffer();
  return await ctx.decodeAudioData(data);
}

const $ = (el, sel) => el.querySelector(sel);
/** @type {HTMLTemplateElement} */
const template = document.getElementById("template");
let holder = document.getElementById("main");
let date = new Date();
let pitch = 1;
let i = 0;

document.getElementById("begin").addEventListener("click", async () => {
  document.getElementById("dialog").remove();
  document.body.className = "begun";

  const ctx = new AudioContext();
  const reverb = ctx.createConvolver();

  function descend(buffer) {
    const fragment = document.importNode(template.content, true);
    const zalgo = summon({ intensity: i / 100 });
    $(fragment, ".date").textContent = zalgo(date.toLocaleDateString());
    $(fragment, ".time").textContent = zalgo(date.toLocaleTimeString());
    $(fragment, ".text").textContent = zalgo("Twitter for Android");
    $(fragment, ".username").textContent = zalgo("jacksfilms");
    $(fragment, ".tag").textContent = zalgo("jacksfilms");
    $(fragment, ".platform").textContent = zalgo("Twitter for Android");
    $(fragment, ".icon").style.filter =
      `invert(${i * 0.33})` +
      `saturate(${1 + i})` +
      `hue-rotate(${-i * 10}deg)`;

    holder.style.transform = `rotate(${(Math.random() - 0.5) * i * 2}deg)`;
    holder.appendChild(fragment);
    holder.scrollIntoView({
      behavior: "smooth",
      block: "center"
    });

    holder = $(holder, ".holder");
    date.setDate(date.getDate() - 1);
    date.setHours(Math.random() * 23, Math.random() * 59, Math.random() * 59);
    i++;

    const node = ctx.createBufferSource();
    node.buffer = buffer;
    node.playbackRate.setValueAtTime(pitch, 0);
    node.connect(reverb);
    node.start(0);
    pitch *= 0.9;
    node.addEventListener("ended", () => {
      descend(buffer);
    });
  }

  const [audioBuffer, impulseBuffer] = await Promise.all([
    parse(ctx, audioURL),
    parse(ctx, impulseURL)
  ]);

  reverb.buffer = impulseBuffer;
  reverb.connect(ctx.destination);

  descend(audioBuffer);
});
